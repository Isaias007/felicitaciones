<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas asociadas

Route::view('/', 'home' );
    
Route::view('/auth/login', 'auth/login');

Route::get('/auth/logout', function () {
    return ("Logout usuario");
});

Route::view('/catalog', '/catalog/index');

Route::get('/catalog/show/{id}', function ($id) {
   return view('/catalog/show', compact("id"));
});

Route::view('/catalog/create', '/catalog/create');

Route::get('/catalog/edit/{id}',  function ($id) {
    return view('/catalog/edit', compact("id"));
});

//Rutas Asociadas

Route::get('/catalog/delete/{id}',  function ($id) {
    return ("Elimina los datos del cliente $id");
});


Route::get('/profile/{name?}', function ($name = "Isaias") {
    return "Este es el perfil de $name";
});

Route::post('/profile/{name}',  function ($name = null) {
    return "Alta de perfil para el usuario: $name";
});

Route::match(['get', 'post'], '/profile/info/{subname}', function ($subname = null) {
    return "Este perfil pertenece a el usuario de apellido: $subname";
});

Route::get('/profile/number/{id}', function ($id) {
    return "Este perfil tiene como id: $id";
})->where('id', '[0-9]+');


Route::get('/profile/{id}/{name}', function ($id, $name) {
    return "Este perfil es de $name con id $id";
})->where(['id' => '[0-9]+', 'name' => '[A-Za-z]+']);

Route::get('/host', function () {
    return env("DB_HOST");
});

Route::get('/timezone', function () {
    return config("app.timezone");
});

//Ejercicio de vistas

Route::view('/inicio', 'home');

// Route::view('/fecha', 'fecha', ["dia"=>date("d"), "mes"=>date("m"), "año"=>date("y")]);

// Route::get('/fecha' ,function(){
// $dia = date("d");
// $mes = date("m");
// $año = date("y");
// return view('fecha', compact("dia", "mes", "año"));

// });

Route::get('/fecha', function () {

return view('fecha')
        ->with("dia", date("d"))
        ->with("mes", date("m"))
        ->with("año", date("y"));
});

Route::view('/prueba', 'prueba');